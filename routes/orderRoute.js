const express = require("express");
const auth = require("../auth");

const router = express.Router();

const orderController = require("../controllers/orderController");

// Route for Checking Out
router.post("/checkOut", (req, res) => {

	let data = {
		userId: req.body.userId,
		productId: req.body.productId,
		quantity: req.body.quantity
	}

	orderController.checkOut(data).then(resultFromController => res.send(resultFromController));
});

// Route for All orders
router.post("/allOrder", auth.verify, (req, res) => {
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	orderController.getAllOrder(data).then(resultFromController => res.send(resultFromController));
})

module.exports = router;