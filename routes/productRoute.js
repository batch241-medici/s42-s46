const express = require("express");
const auth = require("../auth");

const router = express.Router();

const productController = require("../controllers/productController");

// Route for Product Creation
router.post("/createProduct", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	productController.createProduct(data).then(resultFromController => res.send(resultFromController));
});

// Route for all Active Products
router.get("/activeProduct", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for a Specific Product
router.get("/:productId", (req, res) => {

	productController.specificProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for Updating Product Information
router.put("/:productId", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(req.params, data).then(resultFromController => res.send(resultFromController));
});

// Route for Archiving Product
router.patch("/:productId", auth.verify, (req, res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productController.archiveProduct(req.params, data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;