const express = require("express");

const router = express.Router();

const cartController = require("../controllers/cartController");

// Route for Add to Cart
router.route("/addToCart")
.get((req, res, next) => {

	cartController.getAllProduct().then(resultFromController => res.send(resultFromController));
})
.post((req, res, next) => {
	let data = {
		cart: req.body
	}
	cartController.addToCart(data).then(resultFromController => res.send(resultFromController));
})
.patch((req, res, next) => {
	let data = {
		cart: req.body
	}
	cartController.updateCart(data).then(resultFromController => res.send(resultFromController));
})
.patch((req, res, next) => {
	let data = {
		cart: req.body
	}
	cartController.removeItemInCart(data).then(resultFromController => res.send(resultFromController));
})
.post((req, res, next) => {
	let data = {
		cart: req.body
	}
	cartController.totalPrice(data).then(resultFromController => res.send(resultFromController));
})

module.exports = router;