const express = require("express");

const router = express.Router();

const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for User Registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for User Authentication
router.post("/login", (req, res) => {

	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for All Users
router.get("/allUser", (req, res) => {

	userController.allUser().then(resultFromController => res.send(resultFromController));
});

// Route for User's Details
router.post("/userDetail", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.getUserDetail(userData).then(resultFromController => res.send(resultFromController));
})

// Route for User's orders
router.get("/:userId/Order", auth.verify, (req, res) => {
	const data = {
		userId: auth.decode(req.headers.authorization).userId
	}

	userController.getUserOrder(req.params).then(resultFromController => res.send(resultFromController));
})

module.exports = router;