const express = require("express");
const auth = require("../auth");

const router = express.Router();

const adminController = require("../controllers/adminController");

// Route for Admin Registration
router.post("/register", (req, res) => {

	adminController.registerAdmin(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for Admin Login
router.post("/login", (req, res) => {

	adminController.loginAdmin(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for Set user as admin
router.patch("/setAdmin", auth.verify, (req,res) => {
	const data = {
		userId: req.body.userId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	adminController.setAdmin(data).then(resultFromController => res.send(resultFromController));
});



module.exports = router;