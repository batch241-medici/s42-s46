const express = require("express");
const auth = require("../auth");

const Product = require("../models/Product");

const router = express.Router();

const productCategoryController = require("../controllers/productCategoryController");

// Route for Product Category Creation
router.post("/create", auth.verify, (req, res) => {
	const data = {
		productCategory: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};
	productCategoryController.createCategory(data).then(resultFromController => res.send(resultFromController));
});

// Route for Adding Product to category
/*router.post("/putProduct", auth.verify, (req, res) => {
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productCategoryId: req.body.productCategoryId,
		productId: req.body.productId
	};
	productCategoryController.putProduct(data).then(resultFromController => res.send(resultFromController));
});*/

router.route("/putProduct", auth.verify)
.get((req, res, next) => {
	productCategoryController.allCategoryAndProduct().then(resultFromController => res.send(resultFromController))
})
.post((req, res, next) => {
	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		productCategoryId: req.body.productCategoryId,
		products: [
		{
			productId: req.body.products.productId,
			name: req.body.products.name,
			description: req.body.products.description,
			price: req.body.products.price
		}
		]
	}

	productCategoryController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// Route for Getting all categories
router.get("/allCategory", (req, res) => {

	productCategoryController.allCategory().then(resultFromController => res.send(resultFromController));
});

module.exports = router;