const mongoose = require("mongoose");

const productInventorySchema = new mongoose.Schema({
	quantity: {
		type: Number,
		default: [true, "Quantity is required."]
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
})