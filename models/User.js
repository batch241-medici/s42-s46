const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required."]
	},
	password: {
		type: String,
		default: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [
	{
		orderId: {
			type: String,
			required: [true, "Order Id is required."]
		}
	}
	]
})

module.exports = mongoose.model("User", userSchema);