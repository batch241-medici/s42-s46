const mongoose = require("mongoose");

const cartSchema = new mongoose.Schema({
	items: [
	{
		productId: {
			type: String,
			default: [true, "Product Id is required."]
		},
		quantity:{
			type: Number,
			default: [true, "Quantity is required."]
		}
	}
	],
	createdOn: {
		type: Date,
		default: new Date()
	},
	modifiedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("Cart", cartSchema);