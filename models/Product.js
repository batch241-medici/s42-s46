const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		default: [true, "Product name is required."]
	},
	description: {
		type: String,
		default: [true, "Product description is required."]
	},
	price: {
		type: Number,
		default: [true, "Price is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	modifiedOn: {
		type: Date,
		default: new Date()
	},
	inventory: [
	{
		inventoryId: {
			type: String,
			default: [true, "Inventory Id is required."]
		}
	}
	]
});

module.exports = mongoose.model("Product", productSchema)