const mongoose = require("mongoose");

const productCategorySchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required."]
	},
	description: {
		type: String,
		required: [true, "Description is required."]
		
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	modifiedOn: {
		type: Date,
		default: new Date()
	},
	products: [
	{
		productId: {
			type: String,
			
		},
		name: {
			type: String,
			
		},
		description: {
			type: String,
			
		},
		price: {
			type: String,
			
		}
	}
	]
}
);

module.exports = mongoose.model("ProductCategory", productCategorySchema);