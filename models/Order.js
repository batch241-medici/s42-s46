const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId: {
		type: Object,
		default: [true, "User Id is required."]
	},
	products: [
	{
		productId: {
			type: String,
			default: [true, "Product Id is required."]
		},
		quantity: {
			type: Number,
			default: [true, "Quantity is required."]
		}
	}
	],
	totalAmount: {
		type: Number,
		default: [true, "Total Amount is required."]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model("Order", orderSchema);