const mongoose = require("mongoose");

const adminSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required."]
	},
	password: {
		type: String,
		default: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Admin", adminSchema);