const ProductCategory = require("../models/ProductCategory");
const Product = require("../models/Product");
const Admin = require("../models/Admin");

const auth = require("../auth");

// Product Category Creation
module.exports.createCategory = (data) => {
	if(data.isAdmin) {
		let newCategory = new ProductCategory({
			name: data.productCategory.name,
			description: data.productCategory.description
		});
		return newCategory.save().then((result, error) => {
			if(error) {
				return "Error on category creation."
			} else {
				return "Category successfully created."
			}
		})
	};
	let message = Promise.resolve('Must be admin to access this feature.');
	return message.then((value) => {
		return {value};
	});
};

// Add Product to Category
/*module.exports.putProduct = async (data) => {
		
			let doesCategoryExist = await ProductCategory.findById(data.productCategoryId).then(result => {
				result.products.push({productId: data.productId})
				return result.save().then((res, err) => {
					if(err) {
						return false
					} else {
						return true
					}
				})
			})
		

	let doesProductExist = await Product.findById(data.productId).then(result => {
		if(result.length !== null) {
			return true
		} else {
			return false
		}
	})

	
	if(data.isAdmin && doesCategoryExist && doesProductExist) {
		return "Product has been added to category."
	} else {
		return "Must admin to access to this feature."
	}
};*/

// Getting all categories
module.exports.allCategory = () => {
	return ProductCategory.find({}).then(result => {
		return result
	})
};

// 

module.exports.allCategoryAndProduct = async () => {
	let findProductCategory = await ProductCategory.find({});
	let findProduct = await Product.find({});

	return findProductCategory + findProduct
};

module.exports.addProduct = async (data) => {

	let pushInCategory = await ProductCategory.findById(data.productCategoryId).then(result => {
		result.products.push({products: data.products})
		return console.log(result)
		
	})
	
}
