const User = require("../models/User");

const bcrypt = require("bcrypt");
const auth = require("../auth");

// User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return "User already exists."
		} else {
			return newUser.save().then((result, error) => {
				if(error){
					return false
				} else {
					return "Thank you for registering."
				}
			})
		}
	})
};

// User Login
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return "User does not exists."
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				return {message: "Here's your access token:",access: auth.createAccessToken(result)}
			} else {
				return "Password incorrect."
			}
		}
	})
};

// All Users
module.exports.allUser = () => {
	return User.find({}).then(result => {
		return result
	})
};

// User's Details
module.exports.getUserDetail = (userData) => {
	
	return User.findById(userData.id).then(result => {
		return result
	})
}

// User's Orders
module.exports.getUserOrder = (reqParams, data) => {
	
	let
	
}