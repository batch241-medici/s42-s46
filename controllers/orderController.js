const Order = require("../models/Order");
const Product = require("../models/Product");
const User = require("../models/User");
const Admin = require("../models/Admin");

const auth = require("../auth");

// Checking Out
module.exports.checkOut = async (data) => {

	let getPrice = await Product.findById(data.productId).then(result => result.price);
	let isAmount = getPrice * data.quantity;
	

	let newOrder = new Order({
		userId: data.userId,
		products: [
		{
			productId: data.productId,
			quantity: data.quantity
		}
		],
		totalAmount: isAmount
	})

	return newOrder.save().then((result, error) => {
		if(error) {
			return "Error on check-out."
		} else {
			return "Order's been placed."
		}
	})
};

// All Orders
module.exports.getAllOrder = (data) => {
	if(data.isAdmin) {
		return Order.find({})
	} else if(data.isAdmin === false) {
		return false
	}
}