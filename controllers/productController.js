const Admin = require("../models/Admin");
const Product = require("../models/Product");

const auth = require("../auth");

// Product Creation
module.exports.createProduct = (data) => {
	if(data.isAdmin) {
		let newProduct = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price,
		});
		return newProduct.save().then((result, error) => {
			if(error) {
				return "Error on product creation."
			} else {
				result
				return "Product successfully created."
			}
		})
	};
	let message = Promise.resolve('Must be Admin to access this feature.')
	return message.then((value) => {
		return {value};
	});
};

// All Active Products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
};

// Specific Product
module.exports.specificProduct = (productId) => {
	return Product.findById(productId)
};

// Updating Product
module.exports.updateProduct = (reqParams, data) => {
	let updateProduct = {
		name: data.product.name,
		description: data.product.description,
		price: data.product.price
	};

	if(data.isAdmin) {
		return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((result, error) => {
			if(error) {
				return false
			} else {
				return "Product information has been updated."
			}
		})
	} else {
		return "Must be admin to access this feature."
	}
};

// Archiving product
module.exports.archiveProduct = (reqParams, data) => {
	let archiveProduct = {
		isActive: data.product.isActive
	};

	if(data.isAdmin) {
		return Product.findByIdAndUpdate(reqParams.productId, archiveProduct).then((result, error) => {
			if(error) {
				return false
			} else {
				return "Product has been archived."
			}
		})
	} else {
		return "Must be admin to access this feature."
	}
};