const Admin = require("../models/Admin");
const User = require("../models/User");

const bcrypt = require("bcrypt");
const auth = require("../auth");

// Admin Registration
module.exports.registerAdmin = (reqBody) => {
	let newAdmin = new Admin({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return Admin.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return "Admin already exists."
		} else {
			return newAdmin.save().then((result, error) => {
				if(error) {
					return false
				} else {
					return "Thank you for registering."
				}
			})
		}
	})
};

// Admin Login
module.exports.loginAdmin = (reqBody) => {
	return Admin.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return "Admin does not exists."
		} else if (result.isAdmin) {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				return {message: "Welcome! Here's your access token:", access: auth.createAccessToken(result)}
			} else {
				return "Password incorrect."
			}
		} 
	})
};

// Set Admin
module.exports.setAdmin = async (data) => {
	let setToAdmin = await User.findById(data.userId).then(result => {
		let newAdmin = new Admin({
			firstName: result.firstName,
			lastName: result.lastName,
			email: result.email,
			mobileNo: result.mobileNo,
			password: result.password,
			isAdmin: true
		})

		return newAdmin.save().then((res, err) => {
			if(err) {
				return false
			} else {
				return "User's has been granted privileged as admin."
			}
		})
	})
	 return setToAdmin
};