const Cart = require("../models/Cart");
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");

// Get All Product
module.exports.getAllProduct = () => {
	return Product.find({}).then(result => {
		return result;
	})
};

module.exports.addToCart = (data) => {
	let newCart = new Cart({
		items: [
		{
			productId: data.productId,
			quantity: data.quantity
		}
		]
	})

	return newCart.save().then((result, error) => {
		if(error) {
			return "Error on placing product on cart."
		} else {
			return "Product has been successfully placed on cart."
		}
	})
};