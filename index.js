const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoute = require("./routes/userRoute");
const adminRoute = require("./routes/adminRoute");
const productCategoryRoute = require("./routes/productCategoryRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");
const cartRoute = require("./routes/cartRoute");

const port = 4000;

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.set('strictQuery', false);
mongoose.connect("mongodb+srv://admin123:admin123@cluster0.geo96ld.mongodb.net/capstone-2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the Atlas"));

app.use("/users", userRoute);
app.use("/admins", adminRoute);
app.use("/products-category", productCategoryRoute);
app.use("/products-category/products", productRoute);
app.use("/products-category/products/orders", orderRoute);
app.use("/cart", cartRoute);

app.listen(port, () => {
	console.log(`API is now online on port ${port}`)
});